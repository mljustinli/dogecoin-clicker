let UPGRADES = {
    noob: {
        generates: 1,
        cost: 20,
        description:
            "hire a dude who learned how to trade stocks by browsing reddit",
        owned: 0,
    },
    chad: {
        generates: 100,
        cost: 1000,
        description:
            "hire a chad stock trader who went to NYU Stern college of 🅱️usiness",
        owned: 0,
    },
    "dogecoin subreddit": {
        generates: 10000,
        cost: 1000000,
        description:
            "create another subreddit dedicated to the glory of dogecoin",
        owned: 0,
    },
    "discord server": {
        generates: 1000000,
        cost: 100000000,
        description:
            "create a discord server to discuss tomorrow's next moves for dogecoin",
        owned: 0,
    },
    "clickbait-y youtube video": {
        generates: 3300000,
        cost: 999999999,
        description:
            "make a youtube video about how you made millions of dollars from home with a super clickbaity title and red circles and arrows in the thumbnail",
        owned: 0,
    },
    "shipping container mining unit": {
        generates: 420000000,
        cost: 64000000000,
        description:
            "it's a shipping container refitted with 30000 NVIDIA GeForce RTX 30 and at least two times the number of fans",
        owned: 0,
    },
    "quant firm": {
        generates: 13370000000,
        cost: 1337000001337,
        description:
            "a collection MIT and Stanford graduates who have sold their soul for the sake of writing algorithms to mine dogecoin",
        owned: 0,
    },
    "start a cult": {
        generates: 6660000000000,
        cost: 6660000000000000,
        description:
            "start a cult and bully people into joining it, creating an even larger following for dogecoin",
        owned: 0,
    },
};

let RESEARCH = {
    mouse: [
        {
            description:
                "clicking gives 10% of aggregate production instead of 1 bitcoin",
            cost: 2000,
            percentage: 0.1,
        },
        {
            description: "clicking gives 50% of aggregate production",
            cost: 1000000,
            percentage: 0.5,
        },
        {
            description: "clicking gives 100% of aggregate production",
            cost: 1000000000,
            percentage: 1,
        },
        {
            description: "clicking gives 1000% of aggregate production",
            cost: 100000000000000,
            percentage: 10,
        },
    ],
    mining: [
        {
            description: "increase efficiency of all upgrades by x2",
            cost: 990000,
            multiplier: 2,
        },
        {
            description: "increase efficiency of all upgrades by x8",
            cost: 100000000,
            multiplier: 8,
        },
        {
            description: "increase efficiency of all upgrades by x64",
            cost: 100000000000000,
            multiplier: 64,
        },
        {
            description: "increase efficiency of all upgrades by x1024",
            cost: 100000000000000000000000000,
            multiplier: 1024,
        },
    ],
    lab: [
        {
            description: "clicking gives 20% of aggregate production",
            cost: 30000,
        },
        {
            description: "clicking gives 20% of aggregate production",
            cost: 30000,
        },
        {
            description: "clicking gives 20% of aggregate production",
            cost: 30000,
        },
        {
            description: "clicking gives 20% of aggregate production",
            cost: 30000,
        },
    ],
};
