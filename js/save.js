function save(coins, items, researchOwned) {
    localStorage.setItem("coins", coins);

    let itemsDict = {};
    items.forEach((item) => {
        itemsDict[item.name] = item.owned;
    });
    localStorage.setItem("items", JSON.stringify(itemsDict));

    localStorage.setItem("researchOwned", JSON.stringify(researchOwned));
}

function load() {
    let coins = localStorage.getItem("coins");
    if (coins === null) {
        return;
    }

    let itemsDict = JSON.parse(localStorage.getItem("items"));
    if (itemsDict == null) {
        return;
    }

    let researchOwned = JSON.parse(localStorage.getItem("researchOwned"));
    if (researchOwned == null) {
        researchOwned = null;
    }

    return {
        coins: coins,
        items: itemsDict,
        researchOwned: researchOwned,
    };
}
