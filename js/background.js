const DOGE_SPEED = 10;
const POOL_SIZE = 100;
const IMAGE_SIZE = 100;

class Doge {
    constructor(x, y, width, height, image) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.active = false;
    }

    update() {
        if (this.active) {
            this.y += DOGE_SPEED;
            if (this.y - this.height / 2 > canvas.height) {
                this.active = false;
            }
        }
    }

    draw() {
        if (this.active) {
            ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
        }
    }
}

function loadImage(path) {
    let newImage = new Image();
    newImage.style.display = "none";
    newImage.src = path;
    return newImage;
}

let dogeOptions = [
    "img/doge-0.png",
    "img/doge-1.png",
    "img/doge-2.png",
    "img/doge-3.png",
    "img/doge-4.png",
    "img/doge-5.png",
];

let dogeImages = [];
dogeOptions.forEach((doge, index) => {
    dogeImages[index] = loadImage(doge);
});

let canvas;
let ctx;

let dogePool = [];
let lastBackgroundOnState = true;

// https://spicyyoghurt.com/tutorials/html5-javascript-game-development/create-a-proper-game-loop-with-requestanimationframe
window.onload = init;

function init() {
    canvas = document.getElementById("doge-rain-canvas");
    ctx = canvas.getContext("2d");

    createDogePool();

    window.requestAnimationFrame(gameLoop);
}

function gameLoop(timestamp) {
    if (shop.backgroundOn) {
        if (!lastBackgroundOnState) {
            lastBackgroundOnState = true;
        }
        draw();
    } else {
        if (lastBackgroundOnState) {
            lastBackgroundOnState = false;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        }
    }

    window.requestAnimationFrame(gameLoop);
}

function draw() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    if (
        Math.floor(Math.random() * 1000) <
        10 * Math.log10(shop.aggregateProduction)
    ) {
        // let numToAdd = Math.floor(Math.random() * 5);
        // for (let i = 0; i < numToAdd; i++) {
        let doge = getDogeObject();
        doge.x = Math.floor(Math.random() * canvas.width);
        doge.y = -30;
        doge.image = dogeImages[Math.floor(Math.random() * dogeImages.length)];
        // }
    }

    for (let i = 0; i < dogePool.length; i++) {
        let doge = dogePool[i];
        doge.update();
        doge.draw();
    }
}

function createDogePool() {
    for (let i = 0; i < POOL_SIZE; i++) {
        let newDoge = new Doge(0, 0, IMAGE_SIZE, IMAGE_SIZE, dogeImages[0]);
        dogePool.push(newDoge);
    }
}

function getDogeObject() {
    for (let i = 0; i < dogePool.length; i++) {
        let doge = dogePool[i];
        if (!doge.active) {
            doge.active = true;
            return doge;
        }
    }

    let newDoge = new Doge(0, 0, IMAGE_SIZE, IMAGE_SIZE, dogeImages[0]);
    dogePool.push(newDoge);
    return newDoge;
}
