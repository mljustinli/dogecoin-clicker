let SELL_RATIO = 0.8;
let FRAME_RATE = 30;

let savedData = load();
let savedCoins = 0;
let savedResearch = null;

if (savedData) {
    savedCoins = parseFloat(savedData.coins);
    let savedItems = savedData.items;
    Object.entries(savedItems).forEach((pair) => {
        let name = pair[0];
        let owned = pair[1];
        UPGRADES[name].owned = owned;
    });

    savedResearch = savedData.researchOwned;
}

let upgradeArrayForm = [];
for (let upgradeName in UPGRADES) {
    let upgradeCopy = JSON.parse(JSON.stringify(UPGRADES[upgradeName]));
    upgradeCopy.name = upgradeName;
    upgradeArrayForm.push(upgradeCopy);
}

let shop = new Vue({
    el: "#wrapper",
    data: {
        coins: savedCoins,
        items: upgradeArrayForm,
        aggregateProduction: 0,
        backgroundOn: true,
        research: RESEARCH,
        researchOwned:
            savedResearch == null
                ? {
                      // -1 is not upgraded at all, and then max is 3 for all 4 research bought
                      mouse: -1,
                      mining: -1,
                      lab: -1,
                  }
                : savedResearch,
    },
    methods: {
        investButtonClicked: function () {
            if (this.researchOwned.mouse > -1) {
                this.coins +=
                    this.aggregateProduction *
                    this.research.mouse[this.researchOwned.mouse].percentage;
            }
            this.coins++;
        },
        upgrade: function (name) {
            // check if we have enough money
            if (this.coins < UPGRADES[name].cost) {
                return;
            }

            // remove the money
            this.coins -= UPGRADES[name].cost;

            // find index of upgrade
            let index = -1;
            this.items.forEach((item, i) => {
                if (item.name === name) {
                    index = i;
                }
            });

            // modify upgrade to change how many we own
            if (index > -1) {
                let tempObject = this.items[index];
                tempObject.owned++;
                Vue.set(this.items, index, tempObject);

                this.calcAggregateProduction();
            }
        },
        addCommas: function (val) {
            return val.toLocaleString("en-us");
        },
        resetProgress: function () {
            let choice = confirm(
                "Are you sure you want to reset your progress?"
            );
            if (choice == true) {
                clearInterval(saveInterval);
                localStorage.clear();
                location.reload(true);
            }
        },
        calcAggregateProduction: function () {
            let total = 0;
            this.items.forEach((item) => {
                total += item.owned * item.generates;
            });

            if (this.researchOwned.mining > -1) {
                total *= this.research.mining[this.researchOwned.mining]
                    .multiplier;
            }

            this.aggregateProduction = total;
        },
        toggleBackground: function () {
            this.backgroundOn = !this.backgroundOn;
        },
        researchClicked: function (type, index) {
            let cost = this.research[type][index].cost;
            if (this.researchOwned[type] + 1 === index && this.coins >= cost) {
                this.coins -= cost;
                this.researchOwned[type] += 1;

                if (type === "mining") {
                    this.calcAggregateProduction();
                }
            }
        },
    },
});

shop.calcAggregateProduction();

// generate coins every second
setInterval(function () {
    // go through all upgrades and generate coins
    // based on information and how many we own
    // for (let i in shop.items) {
    //     let item = shop.items[i];
    //     shop.coins = shop.coins + (item.generates * item.owned) / FRAME_RATE;
    // }
    shop.coins = shop.coins + shop.aggregateProduction / FRAME_RATE;
}, 1000 / FRAME_RATE);

// save
let saveInterval = setInterval(function () {
    save(shop.coins, shop.items, shop.researchOwned);
}, 1000);
